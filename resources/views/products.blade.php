<!doctype html>
<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
        table{
            border-collapse: collapse;;
        }
        table td{
            border: 1px solid #000;
            padding: 10px;
            text-align: center;
            vertical-align: center;
        }
        </style>
    </head>
    <body>
    <div class="container">
       <div class="col-md-10 col-md-offset-1">
           <table>
               <thead>
                   <tr>
                       <td>Product Name</td>
                       <td>Product Price</td>
                       <td>Product Qty</td>
                       <td>Total Value</td>
                   </tr>
               </thead>
               <tbody>
               <?php 
               $totalvalue = 0;
               ?>
               @foreach($products as $product)
                   <tr>
                    <td>
                        {{$product->product_name}}
                    </td>
                    <td>
                        {{$product->price}}
                    </td>
                    <td>
                        {{$product->quantity}}
                    </td>
                    <td>
                        {{ $product->quantity*$product->price }}
                    </td>
                   </tr>
                   <?php 
                   $totalvalue = $totalvalue + $product->quantity*$product->price;
                   ?>
                @endforeach
               </tbody>
               <tfoot>
                   <tr>
                        <td colspan="3">Total Value</td>
                        <td>{{$totalvalue}}</td>
                   </tr>
               </tfoot>
           </table>
       </div>

       </div>
    </body>
</html>
