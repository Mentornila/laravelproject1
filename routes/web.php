<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('products', 'ProductsController@index');
Route::post('product/create', 'ProductsController@create');
Route::delete('product/destroy/{id}', 'ProductsController@destroy');
Route::put('product/update/{id}', 'ProductsController@update');

//Route::get('/ballina', 'HomeController@index');