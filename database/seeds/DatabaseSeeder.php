<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Product::create([
            'product_name' => 'Coca Cola',
            'quantity' => 10,
            'price' => 5
        ],
        [
            'product_name' => 'Pepsi',
            'quantity' => 10,
            'price' => 5
        ]);
    }
}
